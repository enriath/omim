package com.mapswithme.maps.ads;

import android.support.annotation.NonNull;
import android.view.View;

public interface AdRegistrator
{
  void registerView(@NonNull Object ad, @NonNull View view);
  void unregisterView(@NonNull Object ad, @NonNull View view);
}
